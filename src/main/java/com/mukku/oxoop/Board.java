/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mukku.oxoop;

/**
 *
 * @author acer
 */
public class Board {

    private char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player currPlayer;
    private Player o;
    private Player x;
    private int row;
    private int col;
    private int count;
    private boolean win = false;
    private boolean draw = false;

    public Board(Player o, Player x) {
        this.o = o;
        this.x = x;
        this.currPlayer = o;
        this.count = 0;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrPlayer() {
        return currPlayer;
    }

    public Player getO() {
        return o;
    }

    public Player getX() {
        return x;
    }

    public int getCount() {
        return count;
    }

    public void switchPlayer() {
        if (currPlayer == o) {
            currPlayer = x;
        } else {
            currPlayer = o;
        }
    }

    public boolean isWin() {
        return win;
    }

    public boolean isDraw() {
        return draw;
    }

    public boolean setRowCol(int row, int col) {
        this.row = row;
        this.col = col;
        if (isDraw() || isWin()) return false;
        if (row - 1 >= 0 && row - 1 < table.length && col - 1 >= 0 && col - 1 < table.length && table[row - 1][col - 1] == '-') {
            table[row - 1][col - 1] = currPlayer.getSymbol();
            if (checkWin(row, col)) {
                updateStat();
                this.win = true;
                return true;
            }
            if (checkDraw()) {
                o.draw();
                x.draw();
                this.draw = true;
                return true;
            }
            count++;
            switchPlayer();
            return true;
        }
        return false;
    }

    public void updateStat() {
        if (this.currPlayer == o) {
            o.win();
            x.loss();
        } else {
            x.win();
            o.loss();
        }
    }

    public boolean checkWin(int row, int col) {
        if (checkVertical(col)) {
            return true;
        } else if (checkHorizontal(row)) {
            return true;
        } else if (checkDiagonal()) {
            return true;
        }
        return false;
    }

    public boolean checkVertical(int col) {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkHorizontal(int row) {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkDiagonal() {
        if (checkDiag1()) {
            return true;
        } else if (checkDiag2()) {
            return true;
        }
        return false;
    }

    public boolean checkDiag1() {  // 11, 22, 33
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkDiag2() {  // 13, 22, 31 => 02, 11, 20
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkDraw() {
        if (count == 8) {
            return true;
        }
        return false;
    }

}
